class User < ApplicationRecord
  validates :email, :format => { :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i, :on => :create }
  validates :email, :format => { :with => /id.uff.br\Z/i, :on => :create }
  validates :password, length: { minimum: 6 }
  
  
  enum kind: {
    Student: "0",
    teacher: "1",
    Admin: "2",
    Secretary: "3"
  }

  has_one :student
  has_one :teacher

  devise :database_authenticatable,
         :jwt_authenticatable,
         jwt_revocation_strategy: JwtBlacklist
end

