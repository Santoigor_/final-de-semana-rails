class Classroom < ApplicationRecord
  belongs_to :teacher
  belongs_to :subject
  has_many :subscriptions
  has_many :students, through: :subscriptions
  
  enum status: {
    Desativada: 0,
    Ativada: 1
  }
  
end
