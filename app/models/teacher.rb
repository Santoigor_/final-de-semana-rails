class Teacher < ApplicationRecord
  belongs_to :user
  has_many :teacher_subjets
  has_many :subjects, through: :teacher_subjets

end
