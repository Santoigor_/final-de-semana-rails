class Subject < ApplicationRecord
    has_many :teacher_subjects
    has_many :teachers, through: :teacher_subjects

    has_many :requirements
    has_many :disciplines, through: :requirements, class_name: "Subject"
end
