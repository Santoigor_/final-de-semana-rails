class Requirement < ApplicationRecord
  belongs_to :subjects
  belongs_to :disciplines, class_name: "Subject"
end
