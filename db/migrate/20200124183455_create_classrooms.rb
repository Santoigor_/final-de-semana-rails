class CreateClassrooms < ActiveRecord::Migration[5.2]
  def change
    create_table :classrooms do |t|
      t.string :initials
      t.boolean :monday
      t.boolean :tuesday
      t.boolean :wedsneyda
      t.boolean :thursday
      t.boolean :friday
      t.time :hour
      t.integer :status
      t.references :teacher, foreign_key: true

      t.timestamps
    end
  end
end
