class CreateRequirements < ActiveRecord::Migration[5.2]
  def change
    create_table :requirements do |t|
      t.references :subjects, foreign_key: true
      t.integer :discipline_id

      t.timestamps
    end
  end
end
