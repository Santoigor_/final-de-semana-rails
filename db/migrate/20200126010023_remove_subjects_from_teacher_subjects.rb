class RemoveSubjectsFromTeacherSubjects < ActiveRecord::Migration[5.2]
  def change
    remove_reference :teacher_subjects, :subjects, foreign_key: true
  end
end
