class AddSubjectToTeacherSubjects < ActiveRecord::Migration[5.2]
  def change
    add_reference :teacher_subjects, :subject, foreign_key: true
  end
end
