class CreateSubscriptions < ActiveRecord::Migration[5.2]
  def change
    create_table :subscriptions do |t|
      t.float :p1
      t.float :p2
      t.float :final_grade
      t.integer :status
      t.references :student, foreign_key: true
      t.references :classroom, foreign_key: true

      t.timestamps
    end
  end
end
