Rails.application.routes.draw do
  resources :subscriptions
  resources :requirements
  resources :teacher_subjects
  resources :subjects
  resources :classrooms
  resources :teachers
  resources :students
  
  devise_for :users,
             path: '',
             path_names: {
               sign_in: 'login',
               sign_out: 'logout',
               registration: 'signup'
             },
             controllers: {
               sessions: 'sessions',
               registrations: 'registrations'
             }
end
